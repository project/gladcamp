(function ($) {
  $(document).ready(function(){
    // Initialize expandable blocks.
    $('.block--expands').addClass('is-collapsed').find('.block-title').click(function() {
      $(this).parents('.block--expands').toggleClass('is-expanded is-collapsed');
    });

    if($("#block-views-c35ce757073fc9a536b5805044680fc8, #block-views-c3a5c59240f78fca7e993c94f754bddd").length > 1) {
    	createTabbedSchedule($("#block-views-c35ce757073fc9a536b5805044680fc8, #block-views-c3a5c59240f78fca7e993c94f754bddd"));
    }

  }); // END document.ready

  function createTabbedSchedule(blocks) {
  	var titles = blocks.find(".block-title");
   console.log (titles.name);
  	var menu = $("<ul class='nav-schedule'></ul>");
		  	$(blocks[0]).before(menu);

		blocks.each(function(index){
			var link = $("<a href='#" + $(this).attr("id") + "'>" + $(titles[index]).text() + "</a>");
			if(index == 0) {
				link.addClass("is-active");
			}
			link.addClass("day-" + (index + 1));
			link.appendTo(menu).wrap("<li></li>");
		});

		blocks.each(function(index){
			if(index != 0) {
				$(this).addClass("is-collapsed");
			} else {
				$(this).addClass("is-expanded");
			}
		});

		var menu2 = menu.clone();
		$(blocks[1]).after(menu2);
		menu2.addClass("nav-schedule--bottom");

		$(".nav-schedule a").each(function(){
			$(this).click(function(e){
				blocks.toggleClass("is-expanded is-collapsed");
				$(".nav-schedule a").toggleClass("is-active");
				// e.preventDefault();
			});
		});

		if(window.location.hash.length > 0) {
			$("a[href='" + window.location.hash + "']")[0].click();
		}

  }

})(jQuery); //$
