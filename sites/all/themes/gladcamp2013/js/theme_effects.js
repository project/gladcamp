jQuery(document).ready(function(){
	
	//only perform sprite bg movement effect if IE 9 or more
	if (jQuery.browser.msie && parseInt(jQuery.browser.version, 10) < 9)
	{
		//alternative for IE could go here
		//currently nothing happens
	} else {
		//perform roll over when not active-trail
		jQuery("#zone-menu-wrapper #block-superfish-1 li.sf-depth-1 a").not("#zone-menu-wrapper #block-superfish-1 li.sf-depth-1.active-trail a").hover(
		function(){
			//there are only icons when the width is over 980
			if ( jQuery(window).width() < 980) {
				//set to zero in case the width was changed between roll-overs
				jQuery(this).siblings('.myIconHover').css({opacity:0});
				jQuery(this).siblings('.myIcon').css({opacity:0});
			} else {
				//animate icons to show hover
				jQuery(this).siblings('.myIconHover').animate({opacity:1},500);
				jQuery(this).siblings('.myIcon').animate({opacity:0},500);
				
			}
		}, function() {
			//there are only icons when the width is over 980
			if ( jQuery(window).width() < 980) {
				//catch icons if they were showing between resize to make them back to zero opacity
				jQuery(this).siblings('.myIconHover').css({opacity:0});
				jQuery(this).siblings('.myIcon').css({opacity:0});
			} else {
				//restore to original state
				jQuery(this).siblings('.myIconHover').animate({opacity:0},300);
				jQuery(this).siblings('.myIcon').animate({opacity:1},300);
			}
		});
		
		//mobile menu
		
		jQuery("#zone-menu-wrapper #block-menu-block-5 a").click( function () {
			console.log (this.pathname.replace(/^\//,''));
			//hide all menus
			jQuery('#zone-menu-wrapper #block-menu-block-5 ul ul').hide();
			//now reveal the current menu
			jQuery(this).siblings('ul.menu').toggle();
			//scroll to the top of the clicked menu item after click
			//fix for mobile-bug with jquery and friendly for UX
			jQuery('html, body').animate({
         		scrollTop: jQuery(this).offset().top
     		}, 300);
			//check if hash, if so do not return
			if ((this.pathname.replace(/^\//,'') == "%23") || (this.pathname.replace(/^\//,'') == "gladcamp/%23")) {
				return false;
			}
		} );
		jQuery("#zone-menu-wrapper #block-menu-block-1 a").click( function () {
			//console.log (this.pathname.replace(/^\//,''));
			//hide all menus
			jQuery('#zone-menu-wrapper #block-menu-block-1 ul ul').hide();
			//now reveal the current menu
			jQuery(this).siblings('ul.menu').toggle();
			//scroll to the top of the clicked menu item after click
			//fix for mobile-bug with jquery and friendly for UX
			jQuery('html, body').animate({
         		scrollTop: jQuery(this).offset().top
     		}, 300);
			//check if hash, if so do not return
			if ((this.pathname.replace(/^\//,'') == "%23") || (this.pathname.replace(/^\//,'') == "gladcamp/%23")) {
				return false;
			}
		} );
		
		
	
	}
	
	//hidden eyes roll effect
	jQuery("#eyesOpen").hover(function ()
	{
		jQuery("#eyesOpen").css("background-position","top left");
		setTimeout( function(){
		//must use set timeout with css property
				jQuery("#eyesOpen").css("background-position","bottom left");
				//now remove
				jQuery("#eyesOpen").delay(1200).animate({opacity:0}, 300);
			},500);
	}, function () {
		//no roll out function currently set
	});
	
	if (jQuery(window).width() < 740) {
		if ((location.pathname.replace(/^\//,'') == "") || (location.pathname.replace(/^\//,'') == "gladcamp")) {
			//action to take if home page:
		} else {
			//action to take for other pages:
			jQuery('html, body').delay(1000).animate({
         		scrollTop: jQuery("h1#page-title").offset().top
     		}, 1000, "swing");
		}
	}
	
	//second bg changer
	var bg2 = jQuery('.not-logged-in #secondBG');
	var durVal =2000;
	//looping xfades
	function toRed() {
	bg2.delay(20000).animate({ opacity:1}, durVal, toBlue)
	}
	function toBlue() {
	bg2.delay(20000).animate({opacity:0}, durVal, toRed)
	}
	//initial time is faster than repeat time
	bg2.delay(10000).animate({opacity:1}, durVal, toBlue); 
	
});