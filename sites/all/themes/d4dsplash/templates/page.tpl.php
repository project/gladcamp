<div id="page" class="<?php print $classes; ?>">

  <div id="header">
    <div class="l-constrained">
      <hgroup id="branding">
        <h1>
          <?php if ($logo): ?>
            <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
              <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
            </a>
          <?php else: ?>
            <a href="<?php print $front_page; ?>" rel="home" id="site-name">
             <?php print $site_name; ?>
            </a>
          <?php endif; ?>
        </h1>
      </hgroup>
    </div>
  </div>

  <div id="marquee">
    <div class="marquee--image">
      <img src="/sites/all/themes/d4dsplash/images/bg_stata.jpg" alt="" />
    </div>
    <div class="l-constrained">
      <div class="marquee--date">
        <span>June</span>
        <h2>22-23</h2>
        <span>2013</span>
      </div>
      <div class="marquee--location">
        <h2>MIT</h2>
        <span class="marquee--location-building">Stata Center</span>
        <em>&middot;</em>
        <span class="marquee--location-city">Cambridge, MA</span>
      </div>
      <div class="marquee--about">
        <h2>Design for Drupal, Boston is an annual web design camp covering all aspects of design, UX, &amp; theming for Drupal websites.</h2>
      </div>
      <div class="marquee--intro">
        <p>Since 2009, hundreds of Drupal designers, developers and site builders have converged each year at MIT's Stata Center to share ideas, gather design inspiration, and learn new techniques for Drupal design and theme development from some of the brightest minds in the Drupal design community.</p>
        <p>Your $20 registration fee covers the entire Camp, including coffee and lunches on Saturday and Sunday.</p>
      </div>
    </div>
  </div>
  <div id="above-content">
    <div class="l-constrained">
      <ul class="event-details">
        <li class="event-date">June 22-23, 2013</li>
        <li class="event-location">MIT Stata Center<br />
          <span>(Ray and Maria Stata Center)</span></li>
        <li class="event-address"><p>32 Vassar Street<br />
          Cambridge, MA</p>
          <p class="link-wrapper"><a href="http://goo.gl/maps/a06o1" class="link--more">Google Maps</a></p></li>
      </ul>
    </div>
  </div>
  <div id="main">
    <div class="l-constrained">
      <div class="get-involved">
        <div class="get-involved--volunteer">
          <h2>Volunteer</h2>
          <p>Design for Drupal is seeking volunteers to help out with many aspects of the event, including web design, marketing, event organization, etc. If you are interested in becoming a volunteer please <a href="mailto:hello@design4drupal.org">contact us</a>.</p>
          <p><a href="mailto:hello@design4drupal.org" class="link--primary">Volunteer</a></p>
        </div>
        <div class="get-involved--register">
          <h2>Registration</h2>
          <p>Detailed registration instructions are coming soon! For now, stay up to date by following us on <a href="https://twitter.com/d4dboston">twitter</a> and <a href="https://www.facebook.com/D4DBoston">facebook</a>.</p>
          <ul>
            <li><a href="https://twitter.com/d4dboston" class="link--twitter">Twitter</a></li>
            <li><a href="https://www.facebook.com/D4DBoston" class="link--facebook">Facebook</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
