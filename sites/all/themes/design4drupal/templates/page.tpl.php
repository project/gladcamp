<div id="page" class="<?php print $classes; ?>">

  <div id="header">
    <div class="l-constrained">
      <hgroup id="branding">
        <h1>
          <?php if ($logo): ?>
            <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
              <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
            </a>
          <?php else: ?>
            <a href="<?php print $front_page; ?>" rel="home" id="site-name">
             <?php print $site_name; ?>
            </a>
          <?php endif; ?>
        </h1>

        <?php if ($site_slogan): ?>
          <h3 id="site-slogan">
           <?php print $site_slogan; ?>
          </h3>
        <?php endif; ?>
      </hgroup>
      <?php if ($page['utility']): ?>
        <div id="utility">
          <div class="container">
            <?php print render($page['utility']); ?>
          </div>
        </div>
      <?php endif; // end utility ?>
      <?php if ($page['header']): ?>
          <?php print render($page['header']); ?>
      <?php endif; // end header ?>
    </div>
  </div>

  <?php if ($page['menu_bar'] || $page['navigation']): ?>
    <div id="navigation" class="clearfix">
      <div class="l-constrained">
        <?php print render($page['navigation']); ?>
        <?php print render($page['menu_bar']); ?>
      </div>
    </div>
  <?php endif; // end navigation?>

  <?php if ($messages): ?>
    <div id="messages" class="clearfix">
      <?php print $messages; ?>
    </div>
  <?php endif; // end messages ?>

  <?php if ($page['marquee']): ?>
    <div id="marquee">
      <div class="marquee--image">
        <img src="/sites/all/themes/design4drupal/images/bg_stata.jpg" alt="" />
      </div>
      <div class="l-constrained">
        <?php print render($page['marquee']); ?>
      </div>
    </div> 
  <?php endif; ?>

  <?php if ($page['above_content']): ?>
    <div id="above-content">
      <div class="l-constrained">
        <?php print render($page['above_content']); ?>
      </div>
    </div>
  <?php endif; // end Above Content ?>

  <div id="main">
    <div class="l-constrained">

      <div id="main-content" class="clearfix">

        <div id="content">
          
          <?php if (render($tabs)): ?>
            <div id="tabs">
              <?php print render($tabs); ?>
            </div>
          <?php endif; // end tabs ?>

          <?php if ($page['highlighted']): ?>
            <div id="highlighted">
              <div class="container">
                <?php print render($page['highlighted']); ?>
              </div>
            </div>
          <?php endif; // end highlighted ?>

          <?php if (!$is_front && strlen($title) > 0): ?>
            <h1 <?php if (!empty($title_attributes)) print $title_attributes ?>>
              <?php print $title; ?>
            </h1>
          <?php endif; ?>

          <?php if ($page['help']): ?>
            <div id="help">
              <?php print render($page['help']); ?>
            </div>
          <?php endif; // end help ?>

          <?php if (!$is_front): ?>
            <?php print render($page['content']); ?>
          <?php endif; ?>

        </div>

        <?php if ($page['sidebar_first']): ?>
          <div id="sidebar-first" class="aside">
            <?php print render($page['sidebar_first']); ?>
          </div>
        <?php endif; // end sidebar_first ?>

        <?php if ($page['sidebar_second']): ?>
          <div id="sidebar-second" class="aside">
            <?php print render($page['sidebar_second']); ?>
          </div>
        <?php endif; // end sidebar_second ?>
      </div>

      <?php if ($page['below_content']): ?>
        <div id="below-content">
          <div class="container">
            <?php print render($page['below_content']); ?>
          </div>
        </div>
      <?php endif; // end Below Content ?>

      <?php if ($page['content_aside']): ?>
        <div id="content-aside">
          <div class="container">
            <?php print render($page['content_aside']); ?>
          </div>
        </div>
      <?php endif; // end Content Aside ?>

    </div>
  </div>
  
  <?php if ($page['above_footer']): ?>
    <div id="above-footer">
      <div class="l-constrained">
        <?php print render($page['above_footer']); ?>
      </div>
    </div>
  <?php endif; // end Above Footer ?>

  <div id="footer">
    <div class="l-constrained">
      <?php print render($page['footer']); ?>
    </div>
  </div>

  <?php if ($page['admin_footer']): ?>
    <div id="admin-footer">
      <div class="l-constrained">
        <?php print render($page['admin_footer']); ?>
      </div>
    </div>
  <?php endif; // end admin_footer ?>

</div>
