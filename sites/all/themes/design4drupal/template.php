<?php

/**
 * MENUS
 */

/**
 * Overrides theme_menu_tree() for the main menu.
 */

function design4drupal_menu_tree__menu_block__1($variables) {
  return '<ul class="nav nav-inline nav--dropdown nav--main">' . $variables['tree'] . '</ul>';
}

/**
 * Overrides theme_menu_tree() for the footer menu.
 */

function design4drupal_menu_tree__menu_secondary_menu($variables) {
  return '<ul class="nav nav-inline nav--footer">' . $variables['tree'] . '</ul>';
}

/**
 * Overrides theme_menu_tree() for the social menu.
 */

function design4drupal_menu_tree__menu_block__2($variables) {
  return '<ul class="nav nav-inline nav--social">' . $variables['tree'] . '</ul>';
}

/**
 * Overrides theme_menu_tree() for the utility menu.
 */

function design4drupal_menu_tree__menu_block__3($variables) {
  return '<ul class="nav nav-inline nav--utility">' . $variables['tree'] . '</ul>';
}

/**
 * Overrides theme_menu_tree() for the subnavigation menu.
 */

function design4drupal_menu_tree__menu_block__4($variables) {
  return '<ul class="nav nav--subnav">' . $variables['tree'] . '</ul>';
}

/**
 * Implements hook_preprocess_menu_link()
 */
function design4drupal_preprocess_menu_link(&$vars) {
  /* Set shortcut variables. Hooray for less typing! */
  $menu = $vars['element']['#original_link']['menu_name'];
  $mlid = $vars['element']['#original_link']['mlid'];
  $item_classes = &$vars['element']['#attributes']['class'];
  $link_classes = &$vars['element']['#localized_options']['attributes']['class'];

  /* Add global classes to all menu links */
  $item_classes[] = 'nav-item';
  $link_classes[] = 'nav-link';
  $link_classes[] = 'nav-link-'.$menu;

  switch ($mlid) {
    //  Events
    case 1167:
      $link_classes[] = 'nav-link--social';
      $link_classes[] = 'link--twitter';
      break;
    case 1166:
      $link_classes[] = 'nav-link--social';
      $link_classes[] = 'link--facebook';
      break;
    case 1189:
      $link_classes[] = 'nav-link--utility';
      $link_classes[] = 'link--register';
      break;
    case 1190:
      $link_classes[] = 'nav-link--utility';
      $link_classes[] = 'link--sign-in';
      break;
    case 1191:
      $link_classes[] = 'nav-link--utility';
      $link_classes[] = 'link--sign-out';
      break;
    case 1192:
      $link_classes[] = 'nav-link--utility';
      $link_classes[] = 'link--my-account';
      break;
  }
}

/**
 * FIELDS
 */

/**
 * Implements hook_preprocess_field()
 */
function design4drupal_preprocess_field(&$vars) {
  /* Set shortcut variables. Hooray for less typing! */
  $name = $vars['element']['#field_name'];
  $bundle = $vars['element']['#bundle'];
  $mode = $vars['element']['#view_mode'];
  $classes = &$vars['classes_array'];
  $title_classes = &$vars['title_attributes_array']['class'];
  $content_classes = &$vars['content_attributes_array']['class'];
  $item_classes = array();

  /* Global field styles */
  $classes = array(
    'field',
    'field--' . str_replace('_', '-', $name),
  );
  $title_classes = array('field-label');
  $content_classes = array('field-items');
  $item_classes = array('field-item');

  /* Handle inline label classes */
  if (!$vars['label_hidden'] && $vars['element']['#label_display'] == 'inline') {
    $classes[] = 'field--inline-label';
  }

  /* Uncomment the lines below to see variables you can use to target a field */
  // print '<strong>Name:</strong> ' . $name . '<br/>';
  // print '<strong>Bundle:</strong> ' . $bundle  . '<br/>';
  // print '<strong>Mode:</strong> ' . $mode .'<br/>';

  /* Example: Using an alternative theme function */
  // if($name == 'field_tags') {
  //   $vars['theme_hook_suggestions'][] = 'field__custom_separated';
  // }

  // Apply odd or even classes along with our custom classes to each item */
  foreach ($vars['items'] as $delta => $item) {
    $vars['item_attributes_array'][$delta]['class'] = $item_classes;
    $striping = $delta % 2 ? 'odd' : 'even';
    $vars['item_attributes_array'][$delta]['class'][] = $striping;
  }

  switch ($name) {
    case 'field_profile_first':
    case 'field_profile_last':
      break;
    case 'field_profile_job_title':
    case 'field_profile_org':
      if($mode == 'full') {
        $classes[] = 'field--label-grid';
      }
      break;
    case 'field_profile_interests':
      if($mode == 'full') {
        $classes[] = 'field--label-grid';
        $classes[] = 'field--interests';
      }
      break;
    case 'field_accepted':
    case 'field_track':
    case 'field_experience':
    case 'field_speakers':
      if($mode == 'full') {
        $classes[] = 'field--label-grid';
      }
      break;
    case 'field_what_question_s_does_this_':
      if($mode == 'full') {
        $classes[] = 'field--session-questions';
      }
      break;
    case 'field_ave_you_spoken_at_a_camp_o':
      if($mode == 'full') {
        $classes[] = 'field--speaking-experience';
      }
      break;
  }
}

/**
 * NODES
 */

/**
 * Implements hook_preprocess_node()
 */
function design4drupal_preprocess_node(&$vars) {
  /* Set shortcut variables. Hooray for less typing! */
  $type = $vars['type'];
  $mode = $vars['view_mode'];
  $classes = &$vars['classes_array'];
  $title_classes = &$vars['title_attributes_array']['class'];
  $content_classes = &$vars['content_attributes_array']['class'];

  /* Example: Adding a classes base on View Mode */
  // switch ($mode) {
  //   case 'photo_teaser':
  //     $classes[] = 'bg-white gutters-half l-space-trailing';
  //     break;
  // }
}

/**
 * BLOCKS
 */

/**
 * Implements hook_preprocess_block()
 */
function design4drupal_preprocess_block(&$vars) {
  /* Set shortcut variables. Hooray for less typing! */
  $block_id = $vars['block']->module . '-' . $vars['block']->delta;
  $classes = &$vars['classes_array'];
  $title_classes = &$vars['title_attributes_array']['class'];
  $content_classes = &$vars['content_attributes_array']['class'];

  /* Add global classes to all blocks */
  $title_classes[] = 'block-title';
  $content_classes[] = 'block-content';

  /* Uncomment the line below to see variables you can use to target a block */
  // print $block_id . '<br/>';

  /* Add classes based on the block delta. */
  switch ($block_id) {
    /* System Navigation block */
    // case 'system-navigation':
    //   $classes[] = 'block-rounded';
    //   $title_classes[] = 'block-fancy-title';
    //   $content_classes[] = 'block-fancy-content';
    //   break;

    case 'menu_block-1':
      $classes[] = 'block--menu-main';
      $classes[] = 'block--expands';
      $classes[] = 'block--expands--down';
      $title_classes[] = 'block-title--clickable';
      break;
    case 'menu_block-3':
      $classes[] = 'block--menu-utility';
      break;
    case 'menu_block-4':
      $classes[] = 'block--menu-subnav';
      break;

    //  Marquee
    case 'block-7':
      $classes[] = 'block-marquee';
      break;

    //  Sponsorship
    case 'block-2':
      $classes[] = 'block-half block-sponsorship';
      break;
    //  Propose a Session
    case 'block-3':
      $classes[] = 'block-half block-propose';
      break;
    //  Important Dates
    case 'block-4':
      $classes[] = 'block-reversed';
      break;
    //  Login
    //  Propose a Session
    case 'block-9':
    case 'block-5':
      $classes[] = 'block-gray';
      break;

    //  Sponsors

    case 'views-sponsors-block':
      $classes[] = 'block--sponsors';
      break;
    case 'views-sponsors-block_1':
      $classes[] = 'block--sponsors-design-dev';
      break;
    //  Menu Footer
    case 'menu-menu-secondary-menu':
      $classes[] = 'block-nav--footer';
      break;
    //  Block Menu Social
    case 'menu_block-2':
      $classes[] = 'block-nav--social';
      break;
  }
}

/**
 * FORMS
 */

/**
 * Implements hook_form_alter
 */
function design4drupal_form_alter(&$form, &$form_state, $form_id) {
  /* Add placeholder text to a form */
  if ($form_id == 'search_block_form') {
    $form['search_block_form']['#attributes']['placeholder'] = "Enter a term";
  }
}

/**
 * Implements theme_preprocess_form_element
 */
function design4drupal_preprocess_form_element(&$variables) {
  if ($variables['element']['#type'] == 'radios') {
    $variables['theme_hook_suggestions'][] = 'form_element__description_above';
  }
}

/**
 * Custom implementation of theme_form_element.
 *
 */
function design4drupal_form_element__description_above($variables) {
  $element = &$variables['element'];
  // This is also used in the installer, pre-database setup.
  $t = get_t();

  $description = '';
  if (!empty($element['#description'])) {
    $description .= '<div class="form-item-description">' . $element['#description'] . "</div>\n";
  }

  // This function is invoked as theme wrapper, but the rendered form element
  // may not necessarily have been processed by form_builder().
  $element += array(
    '#title_display' => 'before',
  );

  // Add element #id for #type 'item'.
  if (isset($element['#markup']) && !empty($element['#id'])) {
    $attributes['id'] = $element['#id'];
  }
  // Add element's #type and #name as class to aid with JS/CSS selectors.
  $attributes['class'] = array('form-item');
  if (!empty($element['#type'])) {
    $attributes['class'][] = 'form-type-' . strtr($element['#type'], '_', '-');
  }
  if (!empty($element['#name'])) {
    $attributes['class'][] = 'form-item-' . strtr($element['#name'], array(' ' => '-', '_' => '-', '[' => '-', ']' => ''));
  }
  // Add a class for disabled elements to facilitate cross-browser styling.
  if (!empty($element['#attributes']['disabled'])) {
    $attributes['class'][] = 'form-disabled';
  }
  $attributes['class'][] = 'form-item--description-above';
  $output = '<div' . drupal_attributes($attributes) . '>' . "\n";

  // If #title is not set, we don't display any label or required marker.
  if (!isset($element['#title'])) {
    $element['#title_display'] = 'none';
  }
  $prefix = isset($element['#field_prefix']) ? '<span class="field-prefix">' . $element['#field_prefix'] . '</span> ' : '';
  $suffix = isset($element['#field_suffix']) ? ' <span class="field-suffix">' . $element['#field_suffix'] . '</span>' : '';

  switch ($element['#title_display']) {
    case 'before':
    case 'invisible':
      $output .= ' ' . theme('form_element_label', $variables);
      $output .= $description;
      $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
      break;

    case 'after':
      $output .= ' ' . $prefix . $element['#children'] . $suffix;
      $output .= ' ' . theme('form_element_label', $variables) . "\n";
      $output .= $description;
      break;

    case 'none':
    case 'attribute':
      // Output no label and no required marker, only the children.
      $output .= $description;
      $output .= ' ' . $prefix . $element['#children'] . $suffix . "\n";
      break;
  }

  $output .= "</div>\n";

  return $output;
}

/**
 * Implements theme_status_messages
 */
function design4drupal_status_messages($variables) {
  $display = $variables['display'];
  $output = '';

  $status_heading = array(
    'status' => t('Status message'),
    'error' => t('Error message'),
    'warning' => t('Warning message'),
  );
  foreach (drupal_get_messages($display) as $type => $messages) {
    $output .= "<div class=\"messages $type\"><div class=\"l-constrained\">\n";
    if (!empty($status_heading[$type])) {
      $output .= '<h2 class="element-invisible">' . $status_heading[$type] . "</h2>\n";
    }
    if (count($messages) > 1) {
      $output .= " <ul>\n";
      foreach ($messages as $message) {
        $output .= '  <li>' . $message . "</li>\n";
      }
      $output .= " </ul>\n";
    }
    else {
      $output .= $messages[0];
    }
    $output .= "</div></div>\n";
  }
  return $output;
}

/**
 * COMMENTS
 */

/**
 * Implements theme_preprocess_comment().
 */
function design4drupal_preprocess_comment(&$vars) {
  $user = user_load($vars['comment']->uid);
  $vars['first_name'] = $user->field_profile_first['und'][0]['safe_value'];
  $vars['last_name'] = $user->field_profile_last['und'][0]['safe_value'];
  $vars['posted'] = date('m/d/Y g:ia ', $vars['comment']->created);
}


function design4drupal_node_view_alter(&$build) {
    if ($build['#view_mode'] == 'full') {
        unset($build['links']['comment']['#links']['comment-add']);
    }
}